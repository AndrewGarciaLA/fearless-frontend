window.addEventListener('DOMContentLoaded', async () => {
    const locationURL = 'http://localhost:8000/api/locations/';

    const response = await fetch(locationURL);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        const option = document.createElement('option');
        option.value = location.id
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
    }

    // Handle form submission
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();

      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      try {
        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
          console.log(newConference);
        } else {
          console.error('Failed to create conference');
        }
      } catch (error) {
        console.error('An error occurred:', error);
      }
    });
  });
