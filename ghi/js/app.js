function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    const formattedStartDate = new Date(startDate).toLocaleDateString('en-US')
    const formattedEndDate = new Date(endDate).toLocaleDateString('en-US');
    return `
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary" >${locationName}</h6>
          </div
          <p class="card-text">${description}</p>
          <div class="card-footer">
          <p class="card-text">${formattedStartDate} - ${formattedEndDate}</p>

          </div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error("Houston, we have a problem")
        } else {
            const data = await response.json();

            let count = 0

           // const conference = data.conferences[0];

           // const nameTag = document.querySelector('.card-title');
           // nameTag.innerHTML = conference.name;

            for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = details.conference.starts;
                const endDate = details.conference.ends;
                const locationName = details.conference.location.name
                const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);
                const column = document.querySelectorAll('.col');
                column[count].innerHTML += html;
                count += 1
                if (count === column.length) {
                    count = 0
                }


            }
        }
    }
    } catch (e) {
        console.error("An error occurred:", e);
    }

});
